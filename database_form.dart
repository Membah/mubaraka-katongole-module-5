
import 'dart:developer';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'contactList.dart';

class dataScreen extends StatefulWidget {
  const dataScreen({ Key? key }) : super(key: key);

  @override
  State<dataScreen >createState() => _dataScreenState();
}
class _dataScreenState extends State<dataScreen> {

    TextEditingController namesController = TextEditingController();
    TextEditingController phoneNumberController = TextEditingController();
    TextEditingController ageController = TextEditingController();

    Future _addUser(){
      final names = namesController.text;
      final phoneNumber = phoneNumberController.text;
       final age = ageController.text;

       final ref = FirebaseFirestore.instance.collection("user").doc();
       return ref.set({ "names": names,"phoneNumber": phoneNumber, "age": age, "doc_id":ref.id})
       .then((value) => {

        namesController.text = "",
        phoneNumberController.text = "",
        ageController.text = "",
       }

       )

       
       .catchError((onError) => log(onError));
     }
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
    theme:  ThemeData(
         brightness: Brightness.dark,
       ),
       
     home: Scaffold(
      appBar: AppBar(
        
        title: const Text("User Form"),
      ),
      body: Center(
        
        
          child: ListView(
            children: [
              Column(
               
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Column(
                  children: [
              Padding(padding:const EdgeInsets.symmetric(horizontal: 8,vertical: 8),
              child: TextField(
                controller: namesController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular((20)),
                  ),
                 hintText: "Enter your names:"
                ),
              ),
              ),
              Padding(padding:const EdgeInsets.symmetric(horizontal: 8,vertical: 8),
              child: TextField(
                 controller: phoneNumberController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular((20)),
                  ),
                  hintText: "Enter phone number:"
                ),
              ),
              ),
              Padding(padding:const EdgeInsets.symmetric(horizontal: 8,vertical: 8),
              child: TextField(
                 controller: ageController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular((20)),
                  ),
                  hintText: "Enter your age:"
                ),
              ),
              ),
              ElevatedButton(        
              onPressed: (){_addUser();
              },
              child: const Text('Add User')),
              ],
              
                  ),
                ],
              ), 
             userContactlist(),
            ],
           
          ),
        
      ),
    ),
    );
  }
}