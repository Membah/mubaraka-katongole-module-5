import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'dart:core';

class userContactlist extends StatefulWidget {
  const userContactlist({ Key? key }) : super(key: key);


  @override
  State<userContactlist> createState() => _userContactlistState(); 
}

class _userContactlistState extends State<userContactlist> {
  
  final Stream< QuerySnapshot>_userContacts = 
  FirebaseFirestore.instance.collection('user').snapshots();


  @override
  Widget build(BuildContext context) {

    TextEditingController nameFieldcntroler = TextEditingController();
    TextEditingController phoneNumFieldcntroler = TextEditingController();  
    TextEditingController AgeFieldcntroler = TextEditingController();
    
    void _delete(docId){
    FirebaseFirestore.instance
    .collection("user")
    .doc(docId)
    .delete()
    .then((value) => print("deleted"));
    }

    void _update(data){

      var collection = FirebaseFirestore.instance.collection("user");
      nameFieldcntroler.text = data["names"];
      phoneNumFieldcntroler.text = data["phoneNumber"];
      AgeFieldcntroler.text = data["age"];


    showDialog(context: context, 
    builder: (_) => AlertDialog(
      title: Text("Update"),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          TextField(
        controller:nameFieldcntroler,
        ),
        TextField(
        controller:phoneNumFieldcntroler,
        ),
        
        TextField(
        controller:AgeFieldcntroler,
        ),
        TextButton(
          onPressed: (){
           collection.doc(data["doc_Id"])
           .update({
            "names": nameFieldcntroler.text,
            "phoneNumber": phoneNumFieldcntroler.text,          
            "age": AgeFieldcntroler.text,
            

           });
           Navigator.pop(context);
          },
         child: Text("Update")),
      ]),
    )
    );
    }

    return StreamBuilder(
    
      stream: _userContacts,
      builder: (BuildContext context,
       AsyncSnapshot<QuerySnapshot<Object?>> snapshot){
        if (snapshot.hasError){
          return const Text("Something went wrong");
        }
          if(snapshot.connectionState == ConnectionState.waiting){
          return  const Center (child:CircularProgressIndicator());
        }
       if(snapshot.hasData){
        return Row(
          children: [
           Expanded(
            child: SizedBox(
            height: (MediaQuery.of(context).size.height),
            width: (MediaQuery.of(context).size.width),
            child: ListView(
              children: snapshot.data!.docs
              .map((DocumentSnapshot documentSnapshot)  {
                  Map<String, dynamic> data = documentSnapshot.data()! as Map<
                  String, dynamic>;
                  return Column(
                    children: [
                      Card(
                        child: Column(
                          children:[
                      ListTile(
                        title: Text(data['names']),
                         subtitle:Text(data['phoneNumber']),
                         leading:Text(data['age']), 
                      ),
                      ButtonTheme(
                        child: ButtonBar(
                        children:[
                          OutlineButton.icon(
                            onPressed:(){                             
                              _update(data);
                            },
                            icon: Icon(Icons.edit),
                            label: Text("Edit"),
                            ),
                             OutlineButton.icon(
                            onPressed:(){
                             _delete(data["doc_Id"]);
                            },
                            icon: Icon(Icons.remove),
                            label: Text("Delete"),
                            )
                        ],
                        ),
                      ),
                          ],
                        )
                      )
                    ],
                  );

                
                })
                .toList(),
              
            ),
            ))
          ],
        );

       }else{
        return(Text("No data"));
       }
       },
      
    );
  }
}