// ignore_for_file: file_names
import 'package:flutter/material.dart';
import 'Feature Page2.dart';
import 'Feature page1.dart';
import 'Login Page.dart';
import 'Registration page.dart';
import 'Splash Screen.dart';
import 'User Edit Profile  Page.dart';
import 'database_form.dart';



// ignore: camel_case_types
class dashboardPage extends StatefulWidget {
  const dashboardPage({ Key? key }) : super(key: key);

  @override
  State<dashboardPage> createState() => _dashboardPageState();
}

// ignore: camel_case_types
class _dashboardPageState extends State<dashboardPage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
       theme: ThemeData(
         brightness: Brightness.dark,
       ),
       home: Scaffold(
      appBar: AppBar(
        title: const Text("Dashboard"),
        
      ),
      
      floatingActionButton: FloatingActionButton.extended( label: const Text("REGISTER"),
      onPressed: (){
        Navigator.push(context, MaterialPageRoute(builder: (context) => const registrationPage(),));
      },
      backgroundColor: Colors.green,
      ),
      
      body: Center(
       child: Column(
         //mainAxisAlignment: MainAxisAlignment.center,
         children: [           
           ElevatedButton(
             child: const Text("Login User"),
            onPressed: () {
             Navigator.push(context,MaterialPageRoute(builder: (context) => const LoginPage(), ));
            }
            ),
            const SizedBox(height: 15),
             ElevatedButton(
             child: const Text("Register here"),
            onPressed: () {
             Navigator.push(context,MaterialPageRoute(builder: (context) => const registrationPage(), ));
            }
            ),
             const SizedBox(height: 15),
            ElevatedButton(
             child: const Text("Edit Your Profile"),
            onPressed: () {
               Navigator.push(context,MaterialPageRoute(builder: (context) => const usereditPage(), ));
            }
            ),
               const SizedBox(height: 15),
            ElevatedButton(
             child: const Text("Feature Screen 1"),
            onPressed: () {
                Navigator.push(context,MaterialPageRoute(builder: (context) =>  const featurescreen1Page(), ));
            }
            ),
             const SizedBox(height: 15),
            ElevatedButton(
             child: const Text("Feature Screen 2"),
            onPressed: () {
                Navigator.push(context,MaterialPageRoute(builder: (context) => const featurescreen2Page(), ));
            }
            ),
            const SizedBox(height: 15),
            ElevatedButton(
             child: const Text("Go To Splash Screen"),
            onPressed: () {
                Navigator.push(context,MaterialPageRoute(builder: (context) => const splashScreen(), ));
            }
            ),
            
            const SizedBox(height: 15),
            ElevatedButton(
             child: const Text("Database Form"),
            onPressed: () {
                Navigator.push(context,MaterialPageRoute(builder: (context) => const dataScreen(), ));
            }
            )
            
         ],
       ),
     )
       ) 
    );
  }
}