
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'screens/Dashboard page.dart';
import 'screens/Feature Page2.dart';
import 'screens/Feature page1.dart';
import 'screens/Login Page.dart';
import 'screens/Registration page.dart';
import 'screens/Splash Screen.dart';
import 'screens/User Edit Profile  Page.dart';
import 'screens/database_form.dart';


Future main() async{
  WidgetsFlutterBinding.ensureInitialized();
await Firebase.initializeApp(
  options:
  
  );
  runApp( const MyApp()
  );


}

class MyApp extends StatelessWidget {
  const MyApp({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  MaterialApp(    
      initialRoute: "/splashscreen",
      routes: {
        "/":(context) => const LoginPage(),
        "/dashboard" :(context) => const dashboardPage(),
        "/registration" :(context) => const registrationPage(),
        "/featurescreen1" :(context) => const featurescreen1Page(),
        "/featurescreen2" :(context) => const featurescreen2Page(),
        "/useredit" :(context) => const usereditPage(),
         "/splashscreen" :(context) => const splashScreen(),       
         "/userData" :(context) => const dataScreen(),

       
      },
      
    );
  }
}


